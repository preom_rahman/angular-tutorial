'use strict';

/* Controllers */

angular.module('myApp.controllers', []).
  controller('MyCtrl1', ['$scope', '$sce',  function($scope, $sce) {
      $scope.document = data;
      $scope.trustAsHtml = $sce.trustAsHtml;

  }])
  .controller('MyCtrl2', [function() {

  }]);
