var data = {
  "title": "Django + AngualrJS",
  "chapters": [
    {
      "title": "Introduction",
      "summary": "",
      "sections": [
        {
          "warning": "",
          "tip": "",
          "comment": "",
          "body": "<p>This is a quick guideline on how to use AngularJS and Django together. AngularJS is a client-side JavaScript framework for developing web apps. Django is a server-side web framework in python. Web development with Python/Django has several benefits such as rapid development, good community support (Django has impeccable documentation), and good language structure. AngularJS offers a vast amount of benefits over a combination of vanilla JavaScript or even JQuery. Some of the most important features include better code structure (probably the best and most important feature), two-data binding, testing (as a result of better code structure). It is especially good for CRUD applications, which covers most uses cases for web applications. The benefits of both can be found in better detail on their respective websites.</p>",
          "title": "Quick Summary",
          "$$hashKey": "008"
        },
        {
          "warning": "",
          "tip": "",
          "comment": "",
          "body": "<p>This document is intended to be read from beginning to end, however, certain chapters hold more importance. If the reader is already familiar with Django/Rest (chapter 3) and AngularJS (chapter 2) then they might be able to skip to Chapter 4, the most important chapter, on basic integration. This chapter goes a simple application, and exemplifies some common pitfalls the reader may encounter.</p>\n\n<p>There are probably other tutorials that focus more on AngularJS, however, the chapter in this guide covers what the author thought to be most relevant based on his experience while experimenting with AngularJS. It might especially be helpful given the official AngularJS documentation leaves much to be desired.  It is more likely that  the user is already very familiar with Django, however, the focus in this guide is more on the REST framework because the official django documentation is already so useful. It is still, however, a very short chapter since we only use the back-end for authentication and storage. The later chapters focus</p>",
          "title": "How To Read",
          "$$hashKey": "00B"
        }
      ],
      "$$hashKey": "004"
    },
    {
      "title": "AngularJS Overview",
      "summary": "This chapter will go over a basic AngularJS example and in the process, explain some of the basic components that make up the framework",
      "sections": [
        {
          "warning": "",
          "tip": "",
          "comment": "",
          "body": "<p>Lets take a look at the most bare bones example of an AngularJS app. Head on over to this <a href='http://plnkr.co/edit/fA0SYkQVzHVUdEWo5fAU?p=preview'>code example</a> to see an app that counts the length of a text box. A quick note, this is not the standard way to organize the code, typically the code would at least be in its own script file among other changes. Expect to see the code later on to be a bit differently organized. </p>\n\n<p>An important concept in Angular is that it follows the MVC pattern. If you know what it means, skip this paragraph. The MVC pattern means that the model, view and controller are separate. The model is the representation of any data, the controller is the logic that handles data manipulation and business logic, and finally, the view is the the presentation. In a web app context, the model is typically data from a database retrieved through AJAX, or data from input fields, the controller is the logic that handles AJAX requests, or client side validation, and the view is simply the html code. For more details, refer to the <a href='http://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller'>wikipedia page</a>.</p>",
          "title": "MVC Pattern",
          "$$hashKey": "01O"
        },
        {
          "warning": "",
          "tip": "",
          "comment": "",
          "body": "<p>Let's begin at with the html code, or the view. There are three important things to take note of. \n<ul>\n<li><code>ng-app</code> directive on line 2</li>\n<li><code>ng-controller</code> directive on line 19</li>\n<li><code>ng-model</code> directive on line 22</li>\n<li><code>{{numberLength()}} on line 24</code></li>\n</ul>\n\nThe first three are examples of <b>directives</b>, and the fourth is an example of an expression. Directives are an Angular convention to add behavior to html using a <a href='http://en.wikipedia.org/wiki/Declarative_programming'>declarative syntax</a>. Anything that involves DOM manipulation, or extending html behavior will typically use directives. Angular comes with a set of directives out of the box that take an html element and tie it with with other angular elements of your app such as the controller, models and the app itself! It is quite possible that the last sentence was a bit confusing. We'll look at directives in more detail later. For now, lets focus on the final example of modifying an html element to tie it to other parts of our angular app. </p>\n\n<p>Its great that we've written the html code and we've even written the javascript code for the angular app. However, to tell the html code which app it belongs to, we use the <code>ng-app</code> directive. The directive also applies to the descendants of the element. This also applies to the <code>ng-controller</code> directive, which as the directive name implies, specifies the controller for the section of the html code. The <code>ng-model</code> directive is a bit different and ties an input field to a model. In other words, this creates a model with the name specified by the directive, in this case <code>number</code> as part of the MVC pattern. Next, we'll see how we can access and create models in the controller.</p>",
          "title": "The View",
          "$$hashKey": "02P"
        },
        {
          "warning": "",
          "tip": "",
          "comment": "",
          "body": "<p>Now, lets turn our focus to the javascript code. It's only a scant 7 lines, so its very straightforward. We've declared an angular app using the module method from the angular library, and then we created a controller by calling the controller function on the returned object. Quite often rather than creating a separate variable <code>var app = ... </code> we will just chain the methods like so <code>angular.module(...).controller(...).controller(...)</code>. However it is the author's preference to avoid chaining since it can create slightly ugly code. Angular also comes with a great feature called <b>dependency injection</b> that will let a programmer connect various components of the app by just including the name of the component (such as a service - don't worry we'll see what that is soon) which can make code organization and inclusion very efficient. We'll come back to angular's dependency injection in a moment in this section, and later on in the guide. </p>\n\n<p>Lets take a look at the app declaration, <code> angular.module('myApp', []); </code>, in detail. The <code>module</code> method comes from the angular library which comes from the script file included on line 5. The method takes two arguments: the app name, and a list of dependencies. The app name usage is obvious, we use it in the html declaration to refer to the app code. The list of dependencies is a bit more mysterious. Since our app doesn't have an other dependencies, it's just an empty list. The <code>controller</code> method on the other hand does. As a quick note, the controller method is similar to the app method, except that its list of dependencies ends with a function that is really the controller definition. The other items in the list are the string versions of the parameters passed to the function. In other words, the other item in the list, <code>['$scope'...]</code> is also the first parameter in the controller function definition.  This is so that the code can still work after obfuscation; it is possible to replace the list with just the function, however, it would fail if minimized.</p>\n\n<p>Now lets look at the code for the controller. This brings us to a very important concept: the scope object. We injected the scope object when we declared it as a dependency. The scope object, <code>$scope</code> is what allows the controller and view to communicate. <i>Properties declared on the scope object will be available in the view implicitly.</i> Moreover, properties of the scope are also the data models. The following diagram illustrates how the scope object can be used.</p>\n\n<img src='img/document_images/pic1.png'>\n\n<p>In the diagram we can see that there is a connection using the ng-controller directive, but the communication between the view and the controller happen through the scope object. In the view, the properties are called implicitly <code>first_name</code> as opposed to the controller <code>$scope.first_name</code>. Looking at our code example, we can see that we have a function as a property. The function automatically gets called in the view when ever a property of the scope object it relies on changes. For further details on Angular's event handling, refer to the official documents. <a href='http://docs.angularjs.org/guide/concepts'> This general overview may be a good place to start.</a> </p>",
          "title": "The Controller",
          "$$hashKey": "02S"
        },
        {
          "warning": "",
          "tip": "",
          "comment": "",
          "body": "<p>We've already seen how the model works in Angular as part of the view and the controller. In the view, using the <code>ng-model</code> directive is enough to implicitly create a data model. This data model is also implicitly added as a property to the scope object of the corresponding controller. It is also possible to do the opposite and declare a model in the controller using the <code>$scope</code> object. </p>\n\n<p>An important feature of models in angular is its two-way data-binding. Which basically means that the model representation in both the view and controller are always synchronized. This is an incredibly powerful feature, and means that changes happen instantaneously rather than having to write typical boilerplate code for input changes. The Angular documentation has a good explanation on two-way data-binding and why its so great <a href='http://docs.angularjs.org/guide/dev_guide.templates.databinding'>here</a>.</p>",
          "title": "Model",
          "$$hashKey": "02V"
        },
        {
          "warning": "",
          "tip": "",
          "comment": "",
          "body": "<p>Using 3rd party code can be very beneficial. It can also be a nightmare if the project dies or is poorly maintained. It is therefore important to be careful in using the right type of project to incorporate into the reader's code base. Angular has its own community with important 3rd party libraries that are highly recommended by the author. The official Angular team is sometimes also responsible for these 3rd party libraries making them trustworthy sources of code. Another important reason to use some of these libraries other than code efficiency, is that the libraries are written following the philosophy of Angular.</p>\n\n<p><a href='http://ngmodules.org/'><b>Ng-Modules</b></a> is a good place to begin with an organized collection of angular modules.</p>\n\n<p><a href='http://angular-ui.github.io/'><b>Angular-UI<b></a> offers a good number of ui resources for Angular</p>\n\n<p><a href='https://github.com/angular/angular-seed'><b>Angular-Seed<b></a> Angular Seed is a project that offers a starting template for any Angular project. It is strongly recommended the code organization suggested by Angular Seed in any project for convenience and standards</p>",
          "title": "Third Party Libraries",
          "$$hashKey": "015"
        }
      ],
      "$$hashKey": "017"
    },
    {
      "title": "Django (REST Framework) overview",
      "summary": "This is quick overview on how to use the rest framework in django.",
      "sections": [
        {
          "warning": "",
          "tip": "",
          "comment": "",
          "body": "<p>The <a href='http://django-rest-framework.org/'>Django REST framework</a> is a popular Django framework for creating <a href='http://www.ics.uci.edu/~fielding/pubs/dissertation/rest_arch_style.htm'>restful services</a>. This makes the application scalable, and can be quite easy to get started with a project. </p>\n\n<p>However, the documentation on the Django REST is already excellent and covers common use cases. In fact, I encourage going into the framework in-depth to fully leverage it's power. In this chapter, however, we'll cover a basic example that should cover most use cases to get a project started. This chapter (and guideline) will cover a specific subset of the REST framework that focuses on <a href='http://django-rest-framework.org/api-guide/viewsets.html'>Viewsets</a>. This allows us to leverage a lot of the built-in features that come with the framework for powerful end product with minimal code.</p>",
          "title": "Introduction to REST",
          "$$hashKey": "00N"
        },
        {
          "warning": "",
          "tip": "",
          "comment": "",
          "body": "<p>The first step would be to download and <a href='http://django-rest-framework.org/#installation'>follow the steps to install</a> the framework.</p>\n\n<p>Over the next few sections in this chapter we'll create the back end for a simpel address book system. However, before that, let's discuss a general overview of  how to use the REST framework</p>\n\n<p>Once the REST service is setup, the server and client communicate through standard <a href='http://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Request_methods'>http methods</a> such as <b>POST</b>, <b>DELETE</b>, etc. The service  also follows a specific url structure. The typical structure with a base for a resource is of the form <b><protocol>://<domain name>/<resource name></b>. This base url will return a list of all the resources. This base url can also be used to create a new item. To delete, modify (PUT), or get a single item, and id field can be used after the resource name. The examples in the following table should make it more clear.\n\n<table>\n  <tr>\n    <td>GET <br>\n      http://example.com/clients/</td>\n    <td>Returns a list of all the clients.</td>\n    <td>Returns:<br> [<br>\n      {client_name: 'john', client_id: 0}, <br>\n      {client_name: 'michael', client_id: 1}, <br>\n      ...]\n    </td>\n  </tr>\n   <tr>\n    <td>GET<br>\n      http://example.com/clients/1</td>\n    <td>Returns a single client, michael with id equal to 1 as specified in the url.</td>\n    <td>Returns: <br>\n      {client_name: 'michael', client_id: 1}</td>\n  </tr>\n  <tr>\n    <td>PUT + <i>{client_name:'michelle'}</i><br>\n      http://example.com/clients/1</td>\n    <td>The PUT method (which requires a data parameter) will modify the entry.</td>\n    <td>Changes entry to: <br>\n      {client_name: 'michelle', client_id: 1}</td>\n  </tr>\n</table> <br>\n\n<p>There are other resources on <a href='http://en.wikipedia.org/wiki/Representational_state_transfer#RESTful_web_APIs'>REST applied to a web api</a> that go into more detail. The previous table is just a quick example to give to the reader in the hope that it would make intuitive sense.</p>\n\n<p>The format for exchanging data between the client and server is typically JSON from the server to client. For server to client, there exists various options, we'll use the $.param function from jquery to serialize a deeply nested object to a string form. Angular sends its data in json rather than form encoded data, whereas Django accepts form encoded data. The REST framework accepts both, but for the sake of consistency and improved interchangeability, the $.param function will be used.</p>\n\n<p>The REST framework has 3 - 4 main parts, three of which are part of the standard Django workflow. \n<ul>\n  <li>Registering a router in urls.py, or pointing to the right view class</li>\n  <li>Creating a view, for this example, from a model we've defined.</li>\n  <li>Specifying a serializer class for serializing/deserializing objects. Here we are serializing from internal Django representation of a model to JSON.</li>\n  <li>Specifying a permissions class to see if a user has authorization to perform certain actions.</li>\n</ul>\n\n<p>In addition, a file containing the mode configuration is required. As mentioned already, our views and serializers will be derived from the models we've defined. This reduces the code length, and makes it easy to propogate changes in the app. However, it is possible to write customized views and serializers for more complex tasks. If this is required, reading the official documentation is strongly recommended.</p>",
          "title": "REST Overview",
          "$$hashKey": "00Q"
        },
        {
          "warning": "",
          "tip": "",
          "comment": "",
          "body": "<p>The first thing we'll look at are serializers. Serializers specify how data should be transformed from JSON to python's internal object format.</p>\n\n<p>We'll be using the <a href='http://django-rest-framework.org/api-guide/serializers.html#modelserializer'>ModelSeralizer class</a>.  This will let us create a serializer based on a model we've defined. We'll need to create a new file called <code>serailizers.py</code> in our app directory. To serialize user objects, we would need the following code:\n\n<pre>\n<code>\nfrom rest_framework import serializers\nfrom django.contrib.auth.models import User\n\nclass UserSerializer(serializers.ModelSerializer): \n    class Meta: \n        model = User \n        fields = ('username', 'email')\n</code>\n</pre>\n</p>\n\n<p>That's it! The code itself is very self-explanatory, however, we'll go through it quickly anyways. We import two things: 1) the serializers package from the rest framework and the model we want to serialize. If had defined our own model, we could've imported that as well. We then declare the serializer class that we'll use later on in our viewset (something in the view). This class inherits from the ModelSerializer class from the serializers package. We also need to define a class <code>Meta</code> where we specify the model and fields to use as properties of the class. The rest framework takes care of everything else for us</p>",
          "title": "Serializers",
          "$$hashKey": "01S"
        },
        {
          "warning": "",
          "tip": "",
          "comment": "",
          "body": "<p>Next we need to write a view that will take the serializer and use it based on what is requested. Once again, we'll be using something that can be based on a pre-existing model, the <a href='http://django-rest-framework.org/api-guide/viewsets.html#modelviewset'>ModeViewSet</a>. We'll also be using viewsets, rather than creating typical class based views and using functions from the rest framework. There is a good description of viewsets in the developer guide, in a nuthsell, it uses mixins to provide rest oriented methods such as list, create, etc. The ModelViewset, lets us do this using a model as a base. It also allows us to use the routing mechanism that comes with the rest framework to generate the correct url patterns by just registering the viewset with the router (this is especially helpful given the significance of the base url in rest services).</p>\n\n<p>The code example in the <a href='http://django-rest-framework.org/api-guide/viewsets.html#modelviewset'>ModelViewset</a> section has a very clear code example</p>",
          "title": "Views",
          "$$hashKey": "02X"
        },
        {
          "warning": "",
          "tip": "",
          "comment": "",
          "body": "REST services as a web api rely on the a particular url structure. Implementing this url pattern using django's builtin mehansim from scratch violates the DRY principle. For this reason, it's recommended to use a <b>router</b> object to generate the url patterns. The code, once again, is very straight forward. The following code example is taken from the developer guide on <a href='http://django-rest-framework.org/api-guide/routers.html'>routers</a>\n\n<pre>\n<code>\nfrom rest_framework import routers\n\nrouter = routers.SimpleRouter()\nrouter.register(r'users', UserViewSet)\nrouter.register(r'accounts', AccountViewSet)\nurlpatterns = router.urls\n</code>\n</pre>",
          "title": "Router",
          "$$hashKey": "033"
        },
        {
          "warning": "",
          "tip": "",
          "comment": "",
          "body": "<p>The final thing in this chapter is authentication, or permissions. The rest framework provides an easy mechanism for permissions. There's the option to set <a href='http://django-rest-framework.org/api-guide/permissions.html#setting-the-permission-policy'>default permission settings</a> in the settings file for restrictions such as only permitting the admin or authenticated users. However, we're more interested in customized permissions that can check if the author of an article is the one accessing the article, for example. </p>\n\n<p> It is recommended to create a new file called <code>permissions.py</code> to put the permissions classes. We can then import permissions from the rest framework, and use the <code>permissions.BasePermission</code> class to as a base. We can then create </p>\n\n<p>For details on how to create <a href='http://django-rest-framework.org/api-guide/permissions.html#custom-permissions'>custom permissions</a> refer to the official docs. For a quick example, let's go through a distilled example from the developer guide that checks if the object being accessed by the user is owner. It object has a field called owner as defined in the model. The user is retrieved from the request object thought <a href='https://docs.djangoproject.com/en/dev/topics/auth/default/#auth-web-requests'>django's authentication system</a>.</p>\n\n<pre><code>\nclass IsOwner(permissions.BasePermission):\n\n    def has_object_permission(self, request, view, obj):\n        # Instance must have an attribute named `owner`.\n        return obj.owner == request.user\n</code></pre>\n\n\n<p>Afterwards, in the viewset, we can specify the permissions classes to use by assigning them to the <code>permissions_classes</code> property.</p>\n\n<pre>\n<code>\nfrom permissions import IsOwner\n...\n\nclass UserViewSet(viewsets.ModelViewSet):\n...\n    # Don't forget the comma, as permissions_classes is expected to be an iterable!\n    permissions_classes = (IsOwner,)\n...\n\n</code>\n</pre>",
          "title": "Permissions",
          "$$hashKey": "010"
        }
      ],
      "$$hashKey": "00K"
    },
    {
      "title": "AngualrJS + Django Integration",
      "summary": "This is probably the most important part of this guide. It goes over how to possibly organize a django and angular project together, how to make ajax calls properly, and an overall work flow to get projects started quickly and efficiently.",
      "sections": [
        {
          "warning": "",
          "tip": "",
          "comment": "",
          "body": "<p>The first thing to look at is the folder structure of a project that combines Django and Angular together. We will look at two ways of organizing the projects.</p>\n\n<p><b>Method 1:</b> Typically when using Django, the static files are located in a static folder, and the html files are stored in the templates folder in the app directory, or a general templates folder. To accomplish this, the entire AngularJS project can be placed in the Django app folder and the <code>collectstatic</code> can be run. Before that, we have to make sure that the js files can be found. We can add the path to our js files to <code>STATICFILES_DIRS</code> in the settings.py file. This will collect all the static files and put them in a static folder specified in the settings file. Refer to <a href='https://docs.djangoproject.com/en/dev/howto/static-files/'>managing static files in django</a> for further details. The main problem with this method is that AngularJS expressions use curly braces for expressions (<code>{{<i>expression</i>}}</code>) the same way as Django's templating system. This is easily changed in AngularJS by using the <a href='http://docs.angularjs.org/api/ng.$interpolateProvider'>interpolateProvider</a>. The referenced documentation shows an example as does this <a href-'http://stackoverflow.com/questions/8302928/angularjs-with-django-conflicting-template-tags'>stackoverflow answer</a>. A code example in this guide will be shown momentarily in the next few sections when going through an actual example.</p>\n\n<p><b>Method 2:</b>The way django's static files are handled makes sense for what Django was originally designed for, where a lot of the changes happened in the python files, and the templates used a large amount of Django's templating system. However, we are modifying javascript files frequently and rarely changing the back-end code, it becomes quite tedious to run the <code>collectstatic</code> command every time we make a change. Moreover, having multiple copies of the files is at best annoying at worst it can lead to loss of data by writing to the wrong file and then running collectstatic (collectstaic will overwrite the files collected in the root static directory). Moreover, anytime we wish to add a third party directive, we have to make sure that directive uses the interpolation symbols our code uses. All in all, it becomes quite a hassle. To resolve these problems, it seems better to keep the Angular and Django projects separate and configure the server to forward to the correct folder. This makes the code much more modular.</p>\n\n<p>This tutorial will focus on method 1 for demonstration purposes. There will be a separate, guide on method 2, or an addendum to this guide in the future on method 2</p>",
          "title": "Code/Folder Organization",
          "$$hashKey": "016"
        },
        {
          "warning": "",
          "tip": "",
          "comment": "",
          "body": "<b>Part 1 - Joining AngularJS with Django</b>\n<ol>\n<li>Start a django project in a folder</li>\n<li>Create an app</li>\n<li>Create a templates directory. This can either be a general templates directory specified in the settings file, or a a template folder in the app folder. Create the app's template directory by placing a folder with the app name in the templates directory. If the template directory was placed in the app folder, the structure should look like <i>[project name]/[app name]/templates/[app name]/</i>.</li>\n<li>Inside the app's template directory, copy the app folder of the angular seed project folder</li>\n<li>Create a folder called something like 'static'. A recommended place to put this folder is in the Django project folder (the folder with the settings file).</li>\n<li>In the settings file, set <code>STATIC_ROOT</code> to the static folder path just created.</li>\n<li>Also in the settings file, set the <code>STATIC_URL</code> to '/static/' or something similar </li>\n<li>Open the index file in the angular seed project (the one that is now in app template directory). Modify the reference to each linked file (js,  by adding the static url specified in the settings file. For example, '/js/controllers.js' should become '/static/js/controllers.js'.  </li>\n<li>Open app.js in the js folder in the app folder. Edit the links to partials to also include the static url. For example, 'partials/partial1.html' should become '/static/partials/partial1.html'</li>\n<li>In the app folder edit the view to point to the index page.</li>\n</ol>\n\n<p>This of course doesn't cover ajax or the REST framework, which we'll see in the next section. However, this should give the reader an idea of how to proceed and how to get a basic file served by Django.</p>",
          "title": "Basic Example",
          "$$hashKey": "018"
        },
        {
          "warning": "",
          "tip": "",
          "comment": "",
          "body": "",
          "title": ""
        },
        {
          "warning": "",
          "tip": "",
          "comment": "",
          "body": "",
          "title": ""
        }
      ],
      "$$hashKey": "013"
    }
  ],
  "subtitle": "A Brief Guide",
  "author": "Preom M. Rahman"
};
